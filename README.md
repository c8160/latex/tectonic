# tectonic

Tectonic is a modernized, complete, self-contained TeX/LaTeX engine, powered by
XeTeX and TeXLive.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is tectonic?

Tectonic is a modernized, complete, self-contained TeX/LaTeX engine, powered by
XeTeX and TeXLive.

See [the project website](https://tectonic-typesetting.github.io/en-US/)


## How to use this image

This image is meant to be a one-to-one replacement of a natively compiled and
installed `tectonic`. To compile a document in your current working directory,
call it like this:

```bash
$ podman run --rm -it -v $PWD:$PWD:z -w $PWD registry.gitlab.com/c8160/latex/tectonic:0.9 <file>
```

If you want tectonics cache to persist across usages, add a volume and define
the necessary env var like this:

```bash
$ podman run --rm -it -v $PWD:$PWD:z -w $PWD \
    -e TECTONIC_CACHE_DIR=/var/cache/tectonic \
    -v tectonic-cache:/var/cache/tectonic \
    registry.gitlab.com/c8160/latex/tectonic:0.9
```

Or, if you want a more convenient wrapper, have a look [here][1]

## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/bees/-/issues


[1]: https://gitlab.com/c8160/shell-helpers/-/blob/main/tectonic
