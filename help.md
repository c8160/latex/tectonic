tectonic image, for building latex documents.

Packages the [tectonic Github project](https://gitlab.com/c8160/latex/tectonic)
along with the `biber` executable. `tectonic` is the default entrypoint.

Volumes:
tectonic-cache: You can declare a cache directory for `tectonic` by populating
  the `TECTONIC_CACHE_DIR` env variable. This is completely optional.

Documentation:
For the underlying tectonic project, see https://tectonic-typesetting.github.io/en-US/
For this container, see https://gitlab.com/c8160/latex/tectonic

Requirements:
None

Configuration:
None
