FROM registry.fedoraproject.org/fedora-minimal:39

ENV NAME=tectonic ARCH=x86_64 VERSION=0.15.0
LABEL   name="$NAME" \
        version="$VERSION" \
        architecture="$ARCH" \
        run="podman run --rm -i --log-driver none ..." \
        summary="tectonic in a container" \
        maintainer="Andreas Hartmann <hartan@7x.de>"

COPY LICENSE /
COPY help.md /

RUN curl -LSso "tectonic.tar.gz" "https://github.com/tectonic-typesetting/tectonic/releases/download/tectonic%40$VERSION/tectonic-$VERSION-x86_64-unknown-linux-musl.tar.gz" && \
    microdnf -y install tar gzip; tar -xzf "tectonic.tar.gz"; microdnf -y remove tar gzip && \
    rm "tectonic.tar.gz" && \
    chmod +x tectonic && \
    mv tectonic usr/bin/tectonic
RUN microdnf -y install --setopt=install_weak_deps=0 musl-libc biber && \
    rm -rf /var/cache

ENTRYPOINT [ "/usr/bin/tectonic" ]
